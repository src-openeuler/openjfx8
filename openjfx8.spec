%global openjfxdir %{_jvmdir}/%{name}

%global xmvn_bootstrap 0

	
%ifarch x86_64
%global arch amd64
%else
%global arch aarch64
%endif
%ifarch ppc64le
%global arch ppc64le
%endif
%ifarch loongarch64
%global arch loongarch64
%endif
%ifarch riscv64
%global arch riscv64
%endif

Name:           openjfx8
Version:        8u370
Release:        4
Summary:        Rich client application platform for Java

#fxpackager is BSD
License:        GPL v2 with exceptions and BSD
URL:            https://www.openeuler.org/zh/

Source0:        %{version}.tar.gz
Source1:        README.openeuler
Source2:        pom-base.xml
Source3:        pom-builders.xml
Source4:        pom-controls.xml
Source5:        pom-fxml.xml
Source6:        pom-fxpackager.xml
Source7:        pom-graphics.xml
Source8:        pom-graphics_compileDecoraCompilers.xml
Source9:        pom-graphics_compileDecoraJavaShaders.xml
Source10:       pom-graphics_compileJava.xml
Source11:       pom-graphics_compilePrismCompilers.xml
Source12:       pom-graphics_compilePrismJavaShaders.xml
Source13:       pom-graphics_libdecora.xml
Source14:       pom-graphics_libglass.xml
Source15:       pom-graphics_libglassgtk2.xml
Source16:       pom-graphics_libglassgtk3.xml
Source17:       pom-graphics_libjavafx_font.xml
Source18:       pom-graphics_libjavafx_font_freetype.xml
Source19:       pom-graphics_libjavafx_font_pango.xml
Source20:       pom-graphics_libjavafx_iio.xml
Source21:       pom-graphics_libprism_common.xml
Source22:       pom-graphics_libprism_es2.xml
Source23:       pom-graphics_libprism_sw.xml
Source24:       pom-jmx.xml
Source25:       pom-media.xml
Source26:       pom-openjfx.xml
Source27:       pom-swing.xml
Source28:       pom-swt.xml
Source29:       pom-web.xml
Source30:       shade.xml
Source31:       build.xml
Source32:       buildSrc.xml
Source33:       fxpackager-native.xml
Source34:       fxpackager-so.xml

Patch0:         0000-Fix-wait-call-in-PosixPlatform.patch

ExclusiveArch:  x86_64 aarch64 ppc64le loongarch64 riscv64

Requires:       java-1.8.0-openjdk

BuildRequires:	java-1.8.0-openjdk-devel
BuildRequires:  maven
BuildRequires:	ant
BuildRequires:  gcc
BuildRequires:  gcc-c++
BuildRequires:  libstdc++-static
BuildRequires:  mvn(antlr:antlr)
BuildRequires:  mvn(org.apache.ant:ant)

BuildRequires:  pkgconfig(gtk+-2.0)
BuildRequires:  pkgconfig(gtk+-3.0)
BuildRequires:  pkgconfig(gthread-2.0)
BuildRequires:  pkgconfig(xtst)
BuildRequires:  pkgconfig(libjpeg)
BuildRequires:  pkgconfig(xxf86vm)
BuildRequires:  pkgconfig(gl)
BuildRequires:  mvn(junit:junit)

%description
JavaFX/OpenJFX is a set of graphics and media APIs that enables Java
developers to design, create, test, debug, and deploy rich client
applications that operate consistently across diverse platforms.

The media and web module have been removed due to missing dependencies.

%package devel
Requires: %{name}%{?_isa} = %{version}-%{release}
Requires: java-devel
Summary: OpenJFX development tools and libraries

%description devel
%{summary}.

%package javadoc
Summary: Javadoc for %{name}

%description javadoc
This package contains javadoc for %{name}.

%global debug_package %{nil}

%prep
%setup -q -n openjfx8-%{version}
%patch0 -p1
 
cp %{SOURCE1} .

#Drop *src/test folders
rm -rf modules/{base,builders,controls,fxml,fxpackager,graphics,jmx,media,swing,swt,web}/src/test/
rm -rf buildSrc/src/test/

#prep for graphics
##cp -a modules/javafx.graphics/src/jslc/antlr modules/javafx.graphics/src/main/antlr3
cp -a modules/graphics/src/main/resources/com/sun/javafx/tk/quantum/*.properties modules/graphics/src/main/java/com/sun/javafx/tk/quantum

#prep for base
cp -a modules/base/src/main/java8/javafx modules/base/src/main/java

#prep for swt
cp -a modules/builders/src/main/java/javafx/embed/swt/CustomTransferBuilder.java modules/swt/src/main/java/javafx/embed/swt

find -name '*.class' -delete
find -name '*.jar' -delete

#copy maven files
cp -a %{_sourcedir}/pom-*.xml .
mv pom-openjfx.xml pom.xml

for MODULE in base graphics controls swing swt fxml media web builders fxpackager jmx
do
	mv pom-$MODULE.xml ./modules/$MODULE/pom.xml
done

#shade
mkdir shade
cp -a %{_sourcedir}/shade.xml ./shade/pom.xml

#fxpackager native exe
mkdir ./modules/fxpackager/native
cp -a %{_sourcedir}/fxpackager-native.xml ./modules/fxpackager/native/pom.xml
#fxpackager libpackager.so
mkdir ./modules/fxpackager/so
cp -a %{_sourcedir}/fxpackager-so.xml ./modules/fxpackager/so/pom.xml

cp -a %{_sourcedir}/buildSrc.xml ./buildSrc/pom.xml

mkdir ./modules/graphics/{compileJava,compilePrismCompilers,compilePrismJavaShaders,compileDecoraCompilers,compileDecoraJavaShaders,libdecora,libjavafx_font,libjavafx_font_freetype,libjavafx_font_pango,libglass,libglassgtk2,libglassgtk3,libjavafx_iio,libprism_common,libprism_es2,libprism_sw}
for GRAPHMOD in compileJava compilePrismCompilers compilePrismJavaShaders compileDecoraCompilers compileDecoraJavaShaders libdecora libjavafx_font libjavafx_font_freetype libjavafx_font_pango libglass libglassgtk2 libglassgtk3 libjavafx_iio libprism_common libprism_es2 libprism_sw
do
	mv pom-graphics_$GRAPHMOD.xml ./modules/graphics/$GRAPHMOD/pom.xml
done

#set VersionInfo
cp -a %{_sourcedir}/build.xml .
ant -f build.xml

%build

#set openjdk8 for build
export JAVA_HOME=%{_jvmdir}/java-1.8.0-openjdk
# %%mvn_build
mvn install

%install
install -d -m 755 %{buildroot}%{openjfxdir}
mkdir -p %{buildroot}%{openjfxdir}/bin
mkdir -p %{buildroot}%{openjfxdir}/lib
mkdir -p %{buildroot}%{openjfxdir}/rt/lib/{%{arch},ext}

cp -a shade/target/jfxrt.jar %{buildroot}%{openjfxdir}/rt/lib/ext
cp -a modules/swt/target/jfxswt.jar %{buildroot}%{openjfxdir}/rt/lib
cp -a modules/graphics/libdecora/target/libdecora_sse.so %{buildroot}%{openjfxdir}/rt/lib/%{arch}
cp -a modules/graphics/libglass/target/libglass.so %{buildroot}%{openjfxdir}/rt/lib/%{arch}
cp -a modules/graphics/libglassgtk2/target/libglassgtk2.so %{buildroot}%{openjfxdir}/rt/lib/%{arch}
cp -a modules/graphics/libglassgtk3/target/libglassgtk3.so %{buildroot}%{openjfxdir}/rt/lib/%{arch}
cp -a modules/graphics/libjavafx_font/target/libjavafx_font.so %{buildroot}%{openjfxdir}/rt/lib/%{arch}
cp -a modules/graphics/libjavafx_font_freetype/target/libjavafx_font_freetype.so %{buildroot}%{openjfxdir}/rt/lib/%{arch}
cp -a modules/graphics/libjavafx_font_pango/target/libjavafx_font_pango.so %{buildroot}%{openjfxdir}/rt/lib/%{arch}
cp -a modules/graphics/libjavafx_iio/target/libjavafx_iio.so %{buildroot}%{openjfxdir}/rt/lib/%{arch}
cp -a modules/graphics/libprism_common/target/libprism_common.so %{buildroot}%{openjfxdir}/rt/lib/%{arch}
cp -a modules/graphics/libprism_es2/target/libprism_es2.so %{buildroot}%{openjfxdir}/rt/lib/%{arch}
cp -a modules/graphics/libprism_sw/target/libprism_sw.so %{buildroot}%{openjfxdir}/rt/lib/%{arch}
cp -a modules/jmx/target/javafx-mx.jar %{buildroot}%{openjfxdir}/lib
cp -a modules/fxpackager/target/fxpackager-ant-javafx.jar %{buildroot}%{openjfxdir}/lib/ant-javafx.jar
cp -a modules/fxpackager/target/fxpackager-packager.jar %{buildroot}%{openjfxdir}/lib/packager.jar
cp -a modules/fxpackager/src/main/native/javapackager/shell/javapackager %{buildroot}%{openjfxdir}/bin
cp -a modules/fxpackager/src/main/native/javapackager/shell/javapackager %{buildroot}%{openjfxdir}/bin/javafxpackager

install -d -m 755 %{buildroot}%{_mandir}/man1
install -m 644 modules/fxpackager/src/main/man/man1/* %{buildroot}%{_mandir}/man1

install -d -m 755 %{buildroot}%{_mandir}/ja_JP/man1
install -m 644 modules/fxpackager/src/main/man/ja_JP.UTF-8/man1/* %{buildroot}%{_mandir}/ja_JP/man1

install -d 755 %{buildroot}%{_javadocdir}/%{name}
# cp -a target/site/apidocs/. %{buildroot}%{_javadocdir}/%{name}

mkdir -p %{buildroot}%{_bindir}
ln -s %{openjfxdir}/bin/javafxpackager %{buildroot}%{_bindir}
ln -s %{openjfxdir}/bin/javapackager %{buildroot}%{_bindir}

%files
%dir %{openjfxdir}
%{openjfxdir}/rt
%license LICENSE
%doc README
%doc README.openeuler

%files devel
%{openjfxdir}/lib
%{openjfxdir}/bin
%{_bindir}/javafxpackager
%{_bindir}/javapackager
%{_mandir}/man1/javafxpackager.1*
%{_mandir}/man1/javapackager.1*
%{_mandir}/ja_JP/man1/javafxpackager.1*
%{_mandir}/ja_JP/man1/javapackager.1*
%license LICENSE
%doc README
%doc README.openeuler

%files javadoc
%{_javadocdir}/%{name}
%license LICENSE

%changelog
* Wed Nov 27 2024 Dingli Zhang <dingli@iscas.ac.cn> - 8u370-4
- Add support for riscv64
- Delete settings.xml from source

* Mon Jun 24 2024 songliyang <songliyang@kylinos.cn> - 8u370.3
- Add support for loongarch64
- Fix changelog date error

* Fri Mar 8 2024 Ren Zhijie <zhijie.ren@shingroup.cn> - 8u370.2
- Add support for ppc64le

* Tue Jun 20 2023 DXwangg <wangjiawei80@huawei.com> - 8u370.1
- update 8u353.tar.gz to 8u370.tar.gz

* Mon Dec 12 2022 douyiwang <douyiwang@huawei.com> - 8u353.1
- update 8u352.tar.bz to 8u353.tar.gz

* Wed Dec 7 2022 douyiwang <douyiwang@huawei.com> - 8u352.1
- update 8u340.tar.bz to 8u352.tar.gz

* Tue Jul 19 2022 DXwangg <wangjiawei80@huawei.com> - 8u340.1
- update sqlite from 3.35.3 to 3.37.2

* Tue Jul 12 2022 Noah <hedongbo@huawei.com> - 8u330.1
- change upstream to openeuler
- rm 0004-Fix-Compilation-Flags.patch
- rm 0005-fxpackager-extract-jre-accept-symlink.patch
- rm 0003-fix-cast-between-incompatible-function-types.patch

* Wed Jul 21 2021 Noah <hedongbo@huawei.com> - 8.0,202-26.b07
- change maven repository to huawei cloud

* Sun Jul 11 2021 Benshuai5D <zhangyunbo7@huawei.com> - 8.0.202-26.b07
- add openjfx8.yaml

* Tue Mar 23 2021 Noah <hedongbo@huawei.com> - 8.0.202-25.b07
- change maven repository to ali cloud

* Fri Mar 12 2021 Noah <hedongbo@huawei.com> - 8.0.202-24.b07
- add settings.xml to source

* Thu Dec 24 2020 weidong <weidong@uniontech.com> - 8.0.202-23.b07
- Delete dist macro in spec

* Mon Dec 21 2020 Noah <hedongbo@huawei.com> - 8.0.202-22.b07
- add a license to this repo

* Fri Aug 21 2020 Noah <hedongbo@huawei.com> - 8.0.202-21.b07
- change maven repository to huawei cloud

* Thu Aug 6 2020 Noah <hedongbo@huawei.com> - 8.0.202-20.b07
- Initial packaging
